# smsl_init

Files to add to linux to force play silence wav file to init the (bad) firmware of SMSL A8

Just drop the etc content on you filesystem, and reload udev (or reboot)

The silence.wav (empty) file was created with:

```shell
ffmpeg -ar 48000 -t 0 -f s32le -acodec pcm_s32le -ac 2 -i /dev/zero -acodec copy silence.wav
```
